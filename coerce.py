# coerce.py
# Convert types to other types depending on context.

def to_number(x):
    if x == "":
        return 0
    if isinstance(x, (int, float)):
        return x
    try:
        return int(x)
        # FIXME: should handle floats as well
    except:
        pass
    raise ValueError("could not convert %s to number" % x)

def to_integer(x):
    return int(to_number(x))

def to_string(x):
    return str(x) # TEMPORARY :)

def to_bool(x):
    if isinstance(x, bool): return int(x)
    if x in [0, 0.0, "", [], {}]: return 0
    try:
        if to_integer(x) == 0: return 0
    except:
        pass # could not be coerced to integer
    return 1

