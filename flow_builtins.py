# flow_builtins.py

import collections
import operator
import random
import re
import sys
#
import coerce
import tools

def word(name):
    def _deco_(f):
        f.flow_name = name
        return f
    return _deco_

#
# stack manipulation

@word("dup")
def dup(interp):
    x = interp.pop()
    interp.push(x, x)

@word("drop")
def drop(interp):
    interp.pop()

@word("swap")
def swap(interp):
    """ a b -- b a """
    b = interp.pop(); a = interp.pop()
    interp.push(b, a)

@word("clr")
def clr(interp):
    """ ... -- 
        Clear the stack. """
    interp.stacks[-1] = []

@word("over")
def over(interp):
    """ a b -- a b a """
    x = interp.stacks[-1][-2]
    interp.push(x)

@word("nip")
def nip(interp):
    """ a b -- b """
    del interp.stacks[-1][-2]

@word("dip")
def dip(interp):
    """ ... <blk> x -- ... x 
        Pop x, execute the code block, push x. [taken from Cat]
    """
    x = interp.pop()
    blk = interp.pop(); assert isinstance(blk, tools.CodeBlock)
    interp.process_words(blk.words)
    interp.push(x)

@word("ror")
def ror(interp):
    """ a b c -- c a b """
    c, b, a = [interp.pop() for i in range(3)]
    interp.push(c, a, b)

@word("rol")
def rol(interp):
    """ a b c -- b c a """
    c, b, a = [interp.pop() for i in range(3)]
    interp.push(b, c, a)

#
# output 

@word(".")
def _dot(interp):
    x = interp.pop()
    sys.stdout.write(str(x))

@word("cr")
def cr(interp):
    sys.stdout.write('\n')

#
# arithmetic 

@word("+")
def _plus(interp):
    """ n1 n2 -- n1+n2 """
    n2 = interp.pop()
    n1 = interp.pop()
    interp.push(coerce.to_number(n1) + coerce.to_number(n2)) 

@word("num")
def _num(interp):
    """ x -- n 
        Convert x to a number if possible. Return 0 if no reasonable value can
        be produced. """
    x = interp.pop()
    try:
        n = coerce.to_number(x)
    except ValueError:
        n = 0
    interp.push(n)

#
# code blocks

@word("eval")
def _eval(interp):
    blk = interp.pop()
    assert isinstance(blk, tools.CodeBlock) 
    #print ("Evaluating:", blk.words)
    interp.process_words(blk.words)   

@word("def")
def _def(interp):
    """ <block> <name> -- 
        Creates a new word with the given name and code. """
    name = interp.pop()
    blk = interp.pop()
    assert isinstance(blk, tools.CodeBlock)
    interp.words[name] = blk # a user-defined word is just a CodeBlock
 
#
# generators

def gen_map(it, blk, interp, return_value=True):
    for x in it:
        interp.push(x)
        interp.process_words(blk.words)
        #newval = interp.pop()
        if return_value: # if not set, behaves like a 'foreach'
            newval = interp.pop()
            yield newval

@word("map")
def _map(interp):
    """ <iterator> <codeblock> -- <iterator'> """
    # Note: map expects a code block that takes a value and pushes a value
    # XXX let's assume a list for now; generators later
    blk = interp.pop(); assert isinstance(blk, tools.CodeBlock)
    it = interp.pop()
    interp.push(gen_map(it, blk, interp))

def gen_foreach(interp, it, blk):
    if False: yield 0 # force a generator
    for x in it:
        interp.push(x)
        interp.process_words(blk.words)

@word("foreach")
def foreach(interp):
    """ <iter> <blk> --  
        (FIXME) Like map, but just executes the code block for each value
        retrieved from the initial iterator; does not push a new generator.
        """
    blk = interp.pop()
    it = interp.pop()
    #for x in gen_map(it, blk, interp, False): pass
    for x in gen_foreach(interp, it, blk): pass

@word("filter")
def _filter(interp):
    """ <iter> <blk> -- <iter> """
    blk = interp.pop()
    it = interp.pop()
    def gen_filter():
        for x in it:
            # push x, execute the code block, check the result
            interp.push(x)
            interp.process_words(blk.words)
            result = coerce.to_bool(interp.pop())
            if result:
                yield x
    interp.push(gen_filter())

@word("reduce") # XXX temporary name?
def _reduce(interp):
    """ <it> <default> <blk> -- <value> """
    blk = interp.pop(); assert isinstance(blk, tools.CodeBlock)
    default = interp.pop()
    it = interp.pop()
    # this does not return a generator / no lazy evaluation
    value = default
    for x in it:
        interp.push(x)
        interp.push(value)
        interp.process_words(blk.words)
        value = interp.pop()
    interp.push(value)

@word("range")
def _range(interp):
    """ n -- <gen>
        where <gen> is a generator producing numbers from 0 to n-1. """
    def gen_range(n):
        for i in range(n): yield i
    n = coerce.to_integer(interp.pop()) 
    g = gen_range(n)
    interp.push(g)

@word("list")
def _list(interp):
    """ <gen> -- <list> 
        Extract all values from a generator and push them as a list. """
    it = interp.pop()
    interp.push(list(it))

'''
# may not need this; see 'foreach'
@word("exhaust")
def exhaust(interp):
    """ <gen> -- """
    it = interp.pop()
    for x in it: pass
'''

@word("<>")
def _read_stdin(interp):
    """ -- <gen> """
    def gen_read_stdin():
        while True:
            try:
                s = sys.stdin.readline()
            except:
                raise StopIteration
            else:
                if not s: raise StopIteration
                s = s.rstrip() # strip trailing newline
                parts = s.split()
                # set field vars etc
                interp.record = [s] + parts
                interp.count += 1
                yield s
    interp.push(gen_read_stdin())

@word("take")
def take(interp):
    """ <gen> n -- <gen'> """
    n = coerce.to_number(interp.pop()) # FIXME: to_integer?
    it = iter(interp.pop())
    def gen_take(n):
        for i in range(n):
            yield next(it)
    interp.push(gen_take(n))

@word("skip") 
def skip(interp):
    """ <gen> n -- <gen'> 
        Drop the first <n> elements of the given iterator, then return a new
        generator that will produce the rest of the elements. 
        NOTE: In functional languages this is often called 'drop', but that
        word is already a staple in Forth, hence the different name.
        """
    n = coerce.to_integer(interp.pop())
    it = iter(interp.pop())
    def gen_skip(n):
        for i in range(n): next(it) # discard
        while True:
            yield next(it)
    interp.push(gen_skip(n))

@word("last")
def last(interp):
    """ <it> n -- <it'> 
        Only keep the last n elements of an iterator. """
    n = coerce.to_integer(interp.pop())
    it = iter(interp.pop())
    def gen_last(n):
        all = [x for x in it]
        kept = all[-n:]
        for x in kept:
            yield x
    interp.push(gen_last(n))

@word("enum")
def enum(interp):
    """ <iter> -- <enumerated-iter> """
    it = iter(interp.pop())
    def gen_enum():
        for i, x in enumerate(it):
            yield (x, i)
    interp.push(gen_enum())

def gen_slice(it, first, last):
    for i, x in enumerate(it):
        if i >= first and i <= last:
            yield x
    
@word("slice")
def _slice(interp):
    """ <it> <first> <last> -- <it'> 
        Push a new generator that produces items <first> through <last>
        (inclusive) from an origin iterator.
        NOTE: Currently does not support negative slices.
    """
    last = coerce.to_integer(interp.pop())
    first = coerce.to_integer(interp.pop())
    it = interp.pop()
    interp.push(gen_slice(it, first, last))

@word("slice1")
def _slice1(interp):
    """ <it> <first> <last> -- <it'>
        Like slice, but starts counting at 1. Useful for counting lines. """
    last = coerce.to_integer(interp.pop())
    first = coerce.to_integer(interp.pop())
    it = interp.pop()
    interp.push(gen_slice(it, first-1, last-1))

def gen_takewhile(interp, it, blk, check=True):
    for x in it:
        interp.push(x)
        interp.process_words(blk.words)
        ok = coerce.to_bool(interp.pop())
        if ok != check:
            break
        yield x

@word("take-while")
def takewhile(interp):
    """ <it> <blk> -- <it'> 
        Take from the given iterator as long as its elements match the test
        given in the code block. """
    blk = interp.pop(); assert isinstance(blk, tools.CodeBlock)
    it = interp.pop()
    def gen_takewhile():
        for x in it:
            interp.push(x)
            interp.process_words(blk.words)
            ok = coerce.to_bool(interp.pop())
            if not ok:
                break
            yield x
    interp.push(gen_takewhile())

@word("skip-while")
def skipwhile(interp):
    """ <it> <blk> -- <it'> 
        Skip items from a generator as long as the condition is true. When it
        becomes false, starting yielding. """
    blk = interp.pop(); assert isinstance(blk, tools.CodeBlock)
    it = interp.pop()
    def gen_skipwhile():
        do_yield = False
        for x in it:
            interp.push(x)
            interp.process_words(blk.words)
            ok = coerce.to_bool(interp.pop())
            if not ok:
                do_yield = True
            if do_yield:
                yield x
    interp.push(gen_skipwhile())

#
# regular expressions

@word("~")
def _regex_match(interp):
    """ <str> <regex> -- <match?> 
        Checks if the string matches the given regex. Pushes true (1) if it
        does, otherwise false (0).
        Note: Does not extract any groups.
    """
    regex = interp.pop(); assert isinstance(regex, re._pattern_type)
    s = interp.pop()
    result = coerce.to_bool(bool(regex.search(s)))
    interp.push(result)

@word("~sub")
def _regex_sub(interp):
    """ <str> <regex> <repl> -- <str'> """
    repl = interp.pop()
    regex = interp.pop(); assert isinstance(regex, re._pattern_type)
    s = interp.pop()
    t = regex.sub(repl, s, 1) # only replace first occurrence
    interp.push(t)

@word("~gsub")
def _regex_gsub(interp):
    """ <str> <regex> <repl> -- <str'> """
    repl = interp.pop()
    regex = interp.pop(); assert isinstance(regex, re._pattern_type)
    s = interp.pop()
    t = regex.sub(repl, s) # replace all occurrences
    interp.push(t)

#
# strings and lists

@word("len")
def _len(interp):
    interp.push(len(interp.pop()))
    # for now, assume we can call len() on the object

@word("index")
def index(interp):
    """ <list/string> i -- <elem@i> """
    i = coerce.to_integer(interp.pop())
    lst = interp.pop()
    try:
        x = lst[i]
    except IndexError:
        x = ""
    interp.push(x)

@word("join")
def _index(interp):
    """ <list-of-strings> <separator> -- <str> """
    sep = interp.pop()
    strs = interp.pop()
    strs = [coerce.to_string(s) for s in strs]
    result = sep.join(strs)
    interp.push(result)

@word("scoop")
def scoop(interp):
    """ ... n -- [list-of-n-elems] 
        Take <n> items off the stack and return them as a list.
    """
    n = coerce.to_integer(interp.pop())
    items = list(reversed([interp.pop() for i in range(n)]))
    interp.push(items)

@word("shuf")
def shuf(interp):
    """ """
    stuff = list(interp.pop()) # generator or list
    random.shuffle(stuff) # shuffles in-place
    interp.push(stuff)

# fields/records

@word("fields")
def _fields(interp):
    interp.push(interp.record)

@word("field")
def _field(interp):
    """ n -- $n """
    idx = coerce.to_integer(interp.pop())
    value = interp._get_field(idx)
    interp.push(value)

@word("lineno")
def lineno(interp):
    """ -- n """
    interp.push(interp.count)

#
# comparison, booleans etc

@word("not")
def _not(interp):
    x = coerce.to_bool(interp.pop())
    interp.push(int(not bool(x)))

def make_cmp(name, f):
    @word(name)
    def _cmp_(interp):
        b = coerce.to_number(interp.pop())
        a = coerce.to_number(interp.pop())
        interp.push(int(f(a, b)))
    return _cmp_

_gt = make_cmp(">", operator.gt)
_lt = make_cmp("<", operator.lt)
_ge = make_cmp(">=", operator.ge)
_le = make_cmp("<=", operator.le)
_eq = make_cmp("=", operator.eq)
_ne = make_cmp("!=", operator.ne)

B = coerce.to_bool
_and = make_cmp("and", lambda a, b: B(a) and B(b))
_or = make_cmp("or", lambda a, b: B(a) or B(b))
_xor = make_cmp("xor", lambda a, b: B(a) ^ B(b))

#
# conditionals

@word("if")
def _if(interp):
    """ <cond> <true-block> <false-block> -- ... 
        If <cond> is true, execute <true-block>, otherwise <false-block>.
        What's left on the stack depends on the contents of these blocks.
    """
    falseblk = interp.pop()
    trueblk = interp.pop()
    cond = coerce.to_bool(interp.pop())
    if cond:
        interp.process_words(trueblk.words)
    else:
        interp.process_words(falseblk.words)


#
# meta

@word("words")
def words(interp):
    interp.push(sorted(interp.words))


