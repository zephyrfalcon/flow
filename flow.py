#!/usr/bin/env python3

import getopt
import os
import re
import sys
import traceback
#
import flow_builtins
import tools

class FlowREPL:
    def __init__(self):
        self.fi = FlowInterpreter()
        self.prompt = "> "
    def mainloop(self):
        print("Flow interactive interpreter")
        while True:
            try:
                line = input(self.prompt)
            except EOFError:
                print("\nExiting...")
                break
            try:
                self.fi.process(line)
            except:
                traceback.print_exc()
            self.display_stack()
    def display_stack(self):
        print("Stack: %s" % self.fi.stacks[-1])

re_integer = re.compile("^(-?\d+)$")
def is_integer(s):
    return bool(re_integer.match(s))
    # FIXME: we also want the value of the integer
    # ...or do we?

def is_number(s):
    return is_integer(s) # ...

def is_string_literal(s):
    return s.startswith('"') and s.endswith('"') and len(s) >= 2

def is_regex_literal(s):
    return s.startswith("/") and s.endswith("/") and len(s) >= 2

re_var = re.compile(r"\$(\S+)")
re_field_var = re.compile(r"\$(\d+)")
def is_variable(s):
    return bool(re_var.match(s))

re_word = re.compile(r'("[^"]*")|(/[^/]*/)|(\S+)')

def remove_comments(words):
    try:
        idx = words.index("#")
    except ValueError:
        return words
    return words[:idx]

class FlowInterpreter:

    def __init__(self):
        self.stacks = [[]]
        self.words = {}
        self.variables = {}
        self.record = [] # current line and fields
        self.count = 0

        self._block_level = 0  # if > 0, evaluation is deferred
        self._current_block = []

        self.init()

    def init(self):
        self.load_module(flow_builtins)
        self.load_stdlib()

    def _split(self, line):
        words = []
        g = re_word.finditer(line)
        for match in g:
            start, end = match.span()
            word = line[start:end]
            words.append(word)
        return words

    def process(self, line):
        words = self._split(line) #line.split() 
        words = remove_comments(words)
        self.process_words(words)

    def process_words(self, words):
        for word in words:
            self.process_word(word)

    def process_word(self, word):
        if word == '{':
            self._block_level += 1
        elif word == '}':
            self._block_level -= 1
            if self._block_level == 0:
                blk = tools.CodeBlock(self._current_block[1:])
                self.push(blk)
                self._current_block = []
                return

        if self._block_level > 0:
            self._current_block.append(word)
            return

        if word == '[':
            self.stacks.append([])
        elif word == ']':
            stk = self.stacks.pop()
            self.stacks[-1].append(stk)
        elif is_number(word):
            x = int(word) # for now
            self.push(x)
        elif is_string_literal(word):
            s = eval(word) # !
            self.push(s)
        elif is_regex_literal(word):
            r = re.compile(word[1:-1])
            self.push(r)
        elif is_variable(word):
            self.process_var(word)
        elif word.startswith(">") and is_variable(word[1:]):
            self.process_var_set(word[1:])
        elif word.startswith("#/"):
            # shortcut for map
            sub = ["{", word[2:], "}", "map"]
            self.process_words(sub)
        elif word in self.words:
            f = self.words[word]
            if isinstance(f, tools.CodeBlock):
                self.process_words(f.words)
            else:
                # it's a built-in word
                f(self)
        else:
            raise ValueError("Unknown word: %s" % word)

    def process_var(self, name):
        """ Look up a variable. """
        name = name[1:] # strip leading '$'
        if is_integer(name):
            # field record
            idx = int(name)
            value = self._get_field(idx)
            self.push(value)
        else:
            try:
                value = self.variables[name]
            except KeyError:
                value = self.variables[name] = ""
            self.push(value)

    def _get_field(self, idx):
        try:
            value = self.record[idx] 
        except IndexError:
            value = ""
        return value
    
    def process_var_set(self, name):
        """ Variable assignment, of the form >$foo """
        name = name[1:] # strip leading '$'
        if is_integer(name):
            raise NotImplementedError("field record assignment")
        else:
            newval = self.pop()
            self.variables[name] = newval

    def push(self, *args):
        for arg in args:
            if isinstance(arg, tuple):
                # tuples are yielded by generators to allow pushing of
                # multiple values at a time
                self.stacks[-1].extend(list(arg))
            else:
                self.stacks[-1].append(arg)

    def pop(self):
        x = self.stacks[-1][-1]
        del self.stacks[-1][-1]
        return x

    def top(self):
        return self.stacks[-1][-1]

    def load_module(self, mod):
        for name, value in mod.__dict__.items():
            if hasattr(value, 'flow_name'):
                self.words[value.flow_name] = value

    def load_stdlib(self):
        whereami = os.path.split(os.path.abspath(__file__))[0]
        path = os.path.join(whereami, 'stdlib.flow')
        with open(path) as f:
            lines = f.readlines()
            for line in lines:
                self.process(line)


if __name__ == "__main__":

    interactive = False
    code = None

    opts, args = getopt.getopt(sys.argv[1:], "e:i", [])
    for o, a in opts:
        if o == "-e":
            code = a
        elif o == '-i':
            interactive = True

    if interactive:
        repl = FlowREPL()
        repl.mainloop()
    else:
        fi = FlowInterpreter()
        if code is not None:
            # handle stdin
            fi.process_word("<>")
            fi.process(code)
            # handle stdout
            #print(">>", fi.stacks, file=sys.stderr)
            #input()
            if fi.stacks[-1]: # assume it's a generator
                fi.process("{ . cr } foreach")

