# Flow examples
# (may change over time)

... | flow -e '1 take'
# show the first line

ls -l | flow -e '{ /.py$/ ~ } filter'
# show the .py files in the directory listing

... | flow -e ''
# works like `cat`

cat irclog.txt | flow -e '{ /has joined|has quit/ ~ not } filter'
# filter out noise in IRC logs

# extract battery info for tmux/screen:

    hans@meowstic ~/bitbucket/flow
    % acpi
    Battery 0: Charging, 97%, 00:06:19 until charged

    hans@meowstic ~/bitbucket/flow
    % acpi | ./flow.py -e '1 take { drop $4 /,$/ "" ~sub } map'
    97%

